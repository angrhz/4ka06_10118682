<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>UTS PBWEB</title>
<link rel="stylesheet" href="./style.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round|Open+Sans">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

</head>
<body>

 <header>
        <nav class="navbar navbar-expand-lg navbar-ligt nav p-3">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Pemrograman Berbasis WEB</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
            </div>
        </nav>
    </header> 

	<h2>Silahkan Mengisi Data Anda!</h2>
	 <div class="container">
        <div class="card mt-4 rounded-3 shadow">
            <div class="card-body">
                <h5 class="mt-2 text-center"><strong>Form Pesanan Barang</strong></h5> 
	<form method="POST" action="">
	<table>
		<tr>
			<td>Nama Lengkap</td>
			<td><input type="text" name="nama" placeholder="Masukkan Nama"></td>
		</tr>

		<tr>
			<td>Alamat Lengkap</td>
			<td><input type="text" name="alamat" placeholder="Masukkan Alamat Lengkap"></td>
		</tr>

		<tr>
			<td>Kota</td>
			<td><select name="kota">
			<option value="">---Pilih---</option>
			<option value="Jakarta Utara">Jakarta Utara</option>
			<option value="Jakarta Barat">Jakarta Barat</option>
			<option value="Jakarta Timur">Jakarta Timur</option>
			<option value="Jakarta Selatan">Jakarta Selatan</option>
			<option value="Bekasi">Bekasi</option>
			<option value="Depok">Depok</option>
			<option value="Bogor">Bogor</option>
			<option value="Bandung">Bandung</option>
			<option value="Purwakarta">Purwakarta</option>
			</select></td>
		</tr>

		<tr>
			<td>Provinsi</td>
			<td><select name="provinsi">
			<option value="">---Pilih---</option>
			<option value="DKI Jakarta">DKI Jakarta</option>
			<option value="Jawa Barat">Jawa Barat</option>
			<option value="Jawa Tengah">Jawa Tengah</option>
			<option value="Jawa Timur">Jawa Timur</option>
			
			</select></td>
		</tr>

		<tr>
			<td>Telepon</td>
			<td><input type="text" name="telepon" placeholder="Masukkan Telepon"></td>
		</tr>

		<tr>
			<td>Email</td>
			<td><input type="text" name="email" placeholder="Masukkan Email"></td>
		</tr>

		<tr>
			<td>Nama Bank Pengirim</td>
			<td><select name="nama_bank">
			<option value="">---Pilih---</option>
			<option value="Bank BCA">Bank BCA</option>
			<option value="Bank BRI">Bank BRI</option>
			<option value="Bank Mandiri">Bank Mandiri</option>
			<option value="Bank BNI">Bank BNI</option>
			<option value="Bank Permata">Bank Permata</option>
			</select></td>
		</tr>

		<tr>
			<td>Pemilik Rekening</td>
			<td><input type="text" name="pemilik_rek" placeholder="Masukkan Pemilik Rekening"></td>
		</tr>

		<tr>
			<td>Jumlah Uang</td>
			<td><input type="text" name="jml_uang" placeholder="Masukkan Jumlah Uang"></td>
		</tr>

		<tr>
			<td>Rekening Tujuan</td>
			<td><input type="text" name="rek_tujuan" placeholder="Masukkan Rekening Tujuan"></td>
		</tr>

		<tr>
			<td>Nomor Barang</td>
			<td><input type="text" name="nomor_brg" placeholder="Masukkan Nomor Barang"></td>
		</tr>

		<tr>
			<td>Nama Barang</td>
			<td><select name="nama_brg">
			<option value="">---Pilih---</option>
			<option value="Sepatu Tali Viens">Sepatu Tali Viens</option>
			<option value="Sepatu Wanta 50534">Jakarta Barat</option>
			<option value="Jakarta Timur">Sepatu Wanita 50534</option>
			<option value="Outer Aurora">Auter Aurora</option>
			<option value="T-shirt Polos Wanita">T-shirt Polos Wanita</option>
			<option value="Blose Isabel">Blouse Isabel</option>
			<option value="Kemeja Crop">Kemeja Crop</option>
			</select></td>
		</tr>

		<tr>
			<td>Kode Promo</td>
			<td><input type="text" name="promo" placeholder="Masukkan Kode Promo"></td>
		</tr>

		
	</table>
	<br>	
	<br>
	<div class="col-lg-12 text-center mt-4">
        <button type="submit" name="submit" value="submit" class="btn btn-submit">Simpan</button>
    </div>
    </form>
</div>
</div>
</div>

<br>	
<br>	
<br>	


	  <div class="card mt-4 mb-4 rounded-3 shadow">
            <div class="card-body">
                <h5 class="mt-2 text-center"><strong>Table Pesanan</strong></h5>     
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0" border="1" >
                        <thead class="text-center">
                            <tr>
 
                                <th>Nama Lengkap</th>
                                <th>Alamat Lengkap</th>
                                <th>Kota</th>
                                <th>Provinsi</th>
                                <th>Telepon</th>
                                <th>Email</th>
                                <th>Nama Bank Pengirim</th>
                                <th>Pemilik Rekening</th>
                                <th>Jumlah Uang</th>
                                <th>Rekening Tujuan</th>
                                <th>Nomor Barang</th>
                                <th>Nama Barang</th>
                                <th>Kode Promo</th>

                            </tr>
                        </thead>
                        
                        <tbody>
                            <tr>
                                <td><?php echo $_POST['nama'];?></td>
                                <td><?php echo $_POST['alamat'];?></td>
                                <td><?php echo $_POST['kota'];?></td>
                                <td><?php echo $_POST['provinsi'];?></td>
                                <td><?php echo $_POST['telepon'];?></td>
                                <td><?php echo $_POST['email'];?></td>
                                <td><?php echo $_POST['nama_bank'];?></td>
                                <td><?php echo $_POST['pemilik_rek'];?></td>
                                <td><?php echo $_POST['jml_uang'];?></td>
                                <td><?php echo $_POST['rek_tujuan'];?></td>
                                <td><?php echo $_POST['nomor_brg'];?></td>
                                <td><?php echo $_POST['nama_brg'];?></td>
                                <td><?php echo $_POST['promo'];?></td>
                            </tr>
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
       </div>	
  <br>	
  <br>	

            <p class="mt-2 text-center">Erike Putri Agnesasmitha (12118273)</p>

 
		
</body>
</html>